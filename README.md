# Introduction

The Job Announcement is a web application built in order to showcase a business process-centric
application based on the [Java EE 6](http://www.oracle.com/technetwork/java/javaee/overview/index.html) technology stack
and the [camunda fox BPM Platform](http://www.camunda.com/fox) running on the [JBoss Application Server 7](http://www.jboss.org/jbossas/).
An online version of the showcase can be found at [http://the-job-announcement.com/](http://the-job-announcement.com/).

![The Job Announcement Splash Screen][1]

# We've moved to GitHub!!

The new repository location for this project is on GitHut on [https://github.com/plexiti/the-job-announcement-fox](https://github.com/plexiti/the-job-announcement-fox).

[1]: http://cloud.github.com/downloads/plexiti/the-job-announcement-fox/the-job-announcement-showcase-splash-screen-v3.png
